import io
import sys
from functools import wraps
from typing import Callable

nprint = print


def tprint(*args):
    capturedOutput = io.StringIO()
    sys.stdout = capturedOutput
    nprint(*args)
    sys.stdout = sys.__stdout__
    nprint(capturedOutput.getvalue())


print = tprint


def debug(f: Callable) -> Callable:
    if not hasattr(debug, "fcns"):
        debug.fcns = []
    if f not in debug.fcns:
        debug.fcns.append(f)
        print("[WARNING]: " + repr(f) + " is for debug only")

    @wraps(f)
    def wrapper(*args, **kwargs):
        return f(*args, **kwargs)

    return wrapper