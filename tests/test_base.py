from unittest import TestCase, main

from classdecorators.decorators import DecoratorMetaclass
from utils.Exceptions import InitializationError
from tests.utilities import print


class MetaclassTest(TestCase):
    def setUp(self):
        class BasicDecorator(metaclass=DecoratorMetaclass):
            def __init__(self, **kwargs):
                for key in kwargs:
                    setattr(self, key, kwargs[key])

        class A:
            def __init__(self):
                pass

            def __str__(self):
                return "<class {}>".format(self.__name__)

        class B:
            def __init__(self, arg1):
                self.arg1 = arg1

            def __str__(self):
                return "<class {}>".format(self.__name__)

        self.BasicDecorator = BasicDecorator
        self.A = A
        self.B = B

    def test_assert_decorator_behaviour_when_null_arguments(self):
        self.assertEqual(type(self.BasicDecorator()),
                         type(self.BasicDecorator),
                         "decorator should return its own class")

    def test_decorator_application_returns_decorated_class(self):
        @self.BasicDecorator
        class A:
            pass

        self.assertNotEqual(type(A), type(self.BasicDecorator),
                            "decorated class type equals decorator type")
        self.assertTrue(not isinstance(A(), self.BasicDecorator),
                        "decorated class object is instance of "
                        "decorator class")
        self.assertIsInstance(A(), A, "class does not create instances")

    def test_kwargs_instantiation_initializes_decorator_(self):
        dec = self.BasicDecorator(a=1, b=2)
        dec = dec(a=2)
        dec = dec(a=3)

        self.assertEqual(dec.a, 3, "kwargs fails to initialize")
        self.assertEqual(3, dec.__deccounter__, "shadowclone should contain"
                                                "an incremental counter")
        self.assertTrue(self.BasicDecorator.__name__ == dec.__name__,
                        "decorator should return a shadowclone of "
                        "its own class")

    def test_kwargs_instantiation_creates_shadowclone_of_decorator_class(self):
        dec1 = self.BasicDecorator(a=1, b=2)
        dec2 = dec1(a=2)
        dec3 = dec1(a=3)

        self.assertEqual(type(dec1), type(dec2), "wrong types")
        self.assertEqual(type(dec2), type(dec3), "wrong types")
        self.assertEqual(dec1.__deccounter__, dec3.__deccounter__)
              

    def test_decorator_application_with_kwargs_returns_decorated_class(self):
        @self.BasicDecorator(a=1, b=2)
        class A:
            pass

        a = A()
        self.assertNotEqual(type(A), type(self.BasicDecorator),
                            "decorated class type equals decorator type")
        self.assertTrue(not isinstance(a, self.BasicDecorator),
                        "decorated class object is instance "
                        "of decorator class")

    def test_decorator_rule_preinit(self):
        class Decorator(metaclass=DecoratorMetaclass):
            def __preinit__(self, *args, **kwargs):
                self.a = args[0]
                self.b = kwargs["b"]

        @Decorator
        class A:
            pass

        a = A(0, a=1, b=2)

        self.assertEqual(a.a, 0, "decorator fails to apply args, kwargs")
        self.assertEqual(a.b, 2, "decorator fails to apply args, kwargs")

        @Decorator
        class B:
            def __init__(self):
                self.a = 5
                self.b = 6

        b = B(0, a=1, b=2)
        self.assertEqual(b.a, 5, "preinit overrides user defined init")
        self.assertEqual(b.b, 6, "preinit overrides user defined init")

    def test_decorator_rule_postinit(self):
        class Decorator(metaclass=DecoratorMetaclass):
            def __postinit__(self, *args, **kwargs):
                self.a = args[0]
                self.b = kwargs["b"]

        @Decorator
        class A:
            pass

        @Decorator
        class B:
            def __init__(self):
                self.a = 3
                self.b = 4

        with self.assertRaises(KeyError, msg="postinit should not "
                                             "be able to access kwargs[b]"):
                                                    A(1, 2, 3)

        a = A(1, 2, b=10)
        b = B(6, 7, b=15)

        self.assertEqual(a.a, 1, "postinit could not override "
                                 "instance value a.a")
        self.assertEqual(b.a, 6, "postinit could not override "
                                 "instance variable b.a")
        self.assertEqual(a.b, 10, "postinit could not override "
                                  "instance value a.b")
        self.assertEqual(b.b, 15, "postinit could not override "
                                  "instance variable b.a")

    def test_inherited_decorator_kwargs_preinit_postinit_rules(self):
        class Decorator(self.BasicDecorator):
            def __preinit__(self, *args, **kwargs):
                self.pre = 5

            def __postinit__(self, *args, **kwargs):
                self.post = 7

            def __init__(self, *args, **kwargs):
                self.pre = kwargs["pre"]
                self.post = kwargs["post"]

        @Decorator(pre=3, post=8)
        class A:
            def __init__(self):
                self.pre = 6
                self.post = 6

        self.assertNotEqual(type(A), type(Decorator),
                            "type of decorated class and type of decorator"
                            " should be different")
        a = A()
        self.assertIsInstance(a, A,
                              "decorated class object is not object of class")
        self.assertEqual(a.pre, 6, "preinit failure")
        self.assertEqual(a.post, 7, "postinit failure")

    def test_kwargs_preinit_postinit_rules_when_disabled_in_init(self):
        class Decorator(metaclass=DecoratorMetaclass):
            def __preinit__(self, *args, **kwargs):
                self.pre = 5

            def __postinit__(self, *args, **kwargs):
                self.post = 7

            def __init__(self, *args, **kwargs):
                """Init doc"""
                self.pre = kwargs["pre"]
                self.post = kwargs["post"]
                self.METACLASS_RULES = False

        @Decorator(pre=3, post=8)
        class A:
            def __init__(self):
                self.pre = 6
                self.post = 6

        self.assertNotEqual(type(A), type(Decorator),
                            "type of decorated class and type of decorator"
                            " should be different")
        a = A()
        self.assertIsInstance(a, A,
                              "decorated class object is not object of class")
        self.assertEqual(a.pre, 6, "preinit failure")
        self.assertEqual(a.post, 6, "postinit shouldn't work")

    def test_kwargs_preinit_postinit_rules_when_disabled_in_class(self):
        class Decorator(self.BasicDecorator):

            METACLASS_RULES = False

            def __preinit__(self, *args, **kwargs):
                self.pre = 5

            def __postinit__(self, *args, **kwargs):
                self.post = 7

            def __init__(self, *args, **kwargs):
                self.pre = kwargs["pre"]
                self.post = kwargs["post"]

        @Decorator(pre=3, post=8)
        class A:
            def __init__(self):
                self.pre = 6
                self.post = 6

        self.assertNotEqual(type(A), type(Decorator),
                            "type of decorated class and type of decorator"
                            " should be different")
        a = A()
        self.assertIsInstance(a, A,
                              "decorated class object is not object of class")
        self.assertEqual(a.pre, 6, "preinit failure")
        self.assertEqual(a.post, 6, "postinit failure")

    def test_decorator_is_not_instantiable_if_kwargs_and_not_init(self):
        class Decorator(metaclass=DecoratorMetaclass):
            pass

        with self.assertRaises(InitializationError, msg="an error should be"
                                                        "raised"):
            d = Decorator(pre=3, post=8)
            print(d)

            @Decorator(pre=3, post=8)
            class A:
                def __init__(self):
                    pass

    def test_kwargs_accessibility(self):
        class Decorator(self.BasicDecorator):

            # Do not override __call__. It doesn't work.
            # See notes on commit 87e78136a4f2f0a7a630443ac3a19db6ed05149e
            # def __call__(self, cls):
            #    self.add_kwargs_in_postinit(self)
            #    return cls

            def __preinit__(self, *args, **kwargs):
                self.pre = 1
                self.post = 2

            def pre_apply_kwargs_in_postinit(dec, cls):
                postinit = getattr(dec, "__postinit__",
                                   lambda slf, *args, **kwargs: None)

                def _init_(slf, *args, **kwargs):
                    postinit(slf, *args, **kwargs)
                    # Computation before or after. Freedom!
                    slf.pre = dec.pre
                    slf.post = dec.post
                setattr(dec, "__postinit__", _init_)

        @Decorator(pre=3, post=8)
        class A:
            def __init__(self):
                pass

        a = A()

        self.assertEqual(a.pre, 3, "kwargs not well implemented")
        self.assertEqual(a.post, 8, "kwargs not well implemented")

    def test_decorators_with_2_sets_of_kwargs_are_different(self):
        class Decorator(self.BasicDecorator):
            def pre_apply_kwargs_in_postinit(dec, cls):
                postinit = getattr(dec, "__postinit__",
                                   lambda slf, *args, **kwargs: None)

                def _init_(slf, *args, **kwargs):
                    postinit(slf, *args, **kwargs)
                    # Computation before or after. Freedom!
                    slf.pre = dec.pre
                    slf.post = dec.post
                setattr(dec, "__postinit__", _init_)

        @Decorator(pre=3, post=8)
        class A:
            pass

        @Decorator(pre=1, post=2)
        class B:
            pass

        self.assertNotEqual(A().pre, B().pre, "kwargs must be reimplemented")
        self.assertNotEqual(A().post, B().post,
                            "kwargs must be reimplemented")

    def test_decorators_with_same_kwargs_are_the_same(self):
        class Decorator(self.BasicDecorator):
            def pre_apply_kwargs_in_postinit(dec, cls):
                postinit = getattr(dec, "__postinit__",
                                   lambda slf, *args, **kwargs: None)

                def _init_(slf, *args, **kwargs):
                    postinit(slf, *args, **kwargs)
                    # Computation before or after. Freedom!
                    slf.pre = dec.pre
                    slf.post = dec.post
                setattr(dec, "__postinit__", _init_)

        @Decorator(pre=3, post=8)
        class A:
            pass

        @Decorator(pre=3, post=8)
        class B:
            pass

        self.assertTrue(A.__decorators__.get(Decorator.__name__, 0) is
                        B.__decorators__.get(Decorator.__name__, 0),
                        "the decorators are different")

    def test_same_class_decorators_can_be_accessed_by_decorated_class(self):
        class Decorator(self.BasicDecorator):
            def pre_apply_kwargs_in_postinit(dec, cls):
                postinit = getattr(dec, "__postinit__",
                                   lambda slf, *args, **kwargs: None)

                def _init_(slf, *args, **kwargs):
                    postinit(slf, *args, **kwargs)
                    # Computation before or after. Freedom!
                    slf.pre = dec.pre
                    slf.post = dec.post
                setattr(dec, "__postinit__", _init_)

        @Decorator(pre=3, post=8)
        @Decorator(pre=1, post=4)
        class A:
            pass

        self.assertEqual(len(A.__decorators__), 2,
                         "could not store track of both the decorators")

    def test_decorator_multiplicity_effects(self):
        class Decorator(self.BasicDecorator):
            def pre_apply_kwargs_in_postinit(dec, cls):
                postinit = getattr(dec, "__postinit__",
                                   lambda slf, *args, **kwargs: None)

                def _init_(slf, *args, **kwargs):
                    postinit(slf, *args, **kwargs)
                    # Computation before or after. Freedom!
                    slf.pre = dec.pre
                    slf.post = dec.post
                setattr(dec, "__postinit__", _init_)

        @Decorator(pre=3, post=8)
        @Decorator(pre=1, post=4)
        class A:
            pass

        self.assertEqual(A().pre, 3, "lower decorator overwrites upper")
        self.assertEqual(A().post, 8, "lower decorator overwrites upper")


if __name__ == "__main__":
    main()
