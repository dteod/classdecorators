from classdecorators.base.Decorator import Decorator


class DynamicInitialization(Decorator):
    def __preinit__(self, *args, **kwargs):
        if args:
            attr_args = "args" if "args" not in kwargs else (\
                "_args" if "_args" not in kwargs else (\
                "_" + self.__class__.__name__ + "__args" if "__args" not in kwargs else None))
        else:
            attr_args = "args"
        if not attr_args:
            raise NameError("Avoid definining 'args', '_args' and '__args' named kwargs to avoid data losses")
        else:
            setattr(self, attr_args, args)
        for var in kwargs:
            pvar = "_" + self.__class__.__name__ + var if var.startswith("__")\
                else var
            setattr(self, pvar, kwargs[var])

