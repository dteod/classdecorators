class DecoratorInterface:
    """Farci una docstring paccuta che indica la signature del ConcreteDecorator."""
    __checks__ = []
    __class_variables__ = {}
    __class_methods__ = {}
    __applied_methods__ = []

    def __new__(cls, *args, **kwargs):
        return super(cls.__class__, cls).__new__(cls)

    def __preinit__(self): pass

    def __postinit__(self): pass

