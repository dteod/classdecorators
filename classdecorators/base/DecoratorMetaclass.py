from copy import deepcopy, copy
from typing import Optional
from inspect import signature

from tests.utilities import debug
from .DecoratorInterface import DecoratorInterface
from utils.Exceptions import DecorationError, InitializationError, InstantiationError


class DecoratorMetaclass(type):
    @staticmethod
    def get_shadow(dec, **kwargs) -> DecoratorInterface:
        """This is to instantiate the decorator instead of the class itself.
        Useful if you want to give kwargs to the decorator, to implement
        custom behavior. If you do that, implement __init__(self, **kwargs)
        to initialize the decorator and implement with other methods
        the kwargs logic. By default, the rules here defined
        still apply after the user logic. In case you want to disable them,
        add a class variable
            METACLASS_RULES = False
        in the decorator (or an instance variable in the initializator).
        cached_kwargs = getattr(dec, "__cached_kwargs__", {})
        cached_decs = getattr(dec, "__cached_decs__", {})"""
        if kwargs not in dec.__cached_kwargs__.values():
            dec.__deccounter__ += 1
            d = type(dec).__new__(type(dec), dec.__name__,
                                  dec.__bases__, copy(dict(dec.__dict__)))

            try:
                dec.__init__(d, **kwargs)
            except TypeError:
                raise InitializationError("provide an __init__ method if you "
                                          "want to use kwargs")

            dec.__cached_kwargs__[dec.__deccounter__] = kwargs
            dec.__cached_decs__[dec.__deccounter__] = d
            setattr(dec, "__cached_kwargs__", dec.__cached_kwargs__)
            setattr(dec, "__cached_decs__", dec.__cached_decs__)
            setattr(d, "__cached_kwargs__", dec.__cached_kwargs__)
            setattr(d, "__cached_decs__", dec.__cached_decs__)
            setattr(dec, "__deccounter__", dec.__deccounter__)
            setattr(d, "__deccounter__", dec.__deccounter__)

            return d
        else:
            for key in dec.__cached_kwargs__.keys():
                if dec.__cached_kwargs__[key] == kwargs:
                    return dec.__cached_decs__[key]

    @staticmethod
    def apply_pre(dec, cls):
        pre = [getattr(dec, attr)
               for attr in dec.__dict__ if attr.startswith("pre_")]
        pre.sort(key=lambda x: x.__name__)
        for method in pre:
            method(dec, cls)

    @staticmethod
    def set_class_variables(dec, cls):
        for var in getattr(dec, "__class_variables__", []):
            pvar = "_" + cls.__name__ + var if var.startswith("__") else var
            if not getattr(cls, pvar, None):
                try:
                    setattr(cls, pvar,
                            deepcopy(dec.__class_variables__[var]))
                except TypeError:
                    try:
                        setattr(cls, pvar,
                                copy(dec.__class_variables__[var]))
                    except TypeError:
                        setattr(cls, pvar, dec.__class_variables__[var])

    @staticmethod
    def set_hidden_methods(dec, cls):
        for var in getattr(dec, "__hidden_methods__", []):
            pvar = "_" + cls.__name__ + var if var.startswith("__") else var
            if not getattr(cls, pvar, None):
                setattr(cls, pvar, dec.__hidden_methods__[var])

    @staticmethod
    def override_methods(dec, cls):
        for var in getattr(dec, "__override_methods__", []):
            pvar = "_" + cls.__name__ + var if var.startswith("__") else var
            setattr(cls, pvar, dec.__override_methods__[var])

    @staticmethod
    def override_new(dec, cls):
        DecoratorMetaclass.test_token_instantiation(dec)
        setattr(cls, "__new__", dec.__class_new__)

    @staticmethod
    @debug
    def test_token_instantiation(dec):
        Token = type("Token", (object,), dict())

        DecoratorMetaclass.apply_pre(dec, Token)
        DecoratorMetaclass.set_class_variables(dec, Token)
        DecoratorMetaclass.set_hidden_methods(dec, Token)
        DecoratorMetaclass.override_methods(dec, Token)

        try:
            if not isinstance(dec.__class_new__(Token), Token):
                raise DecorationError("the __class_new__ method "
                                      "must return an instance "
                                      "of the decorated class")
        except InstantiationError as e:
            if str(e) == "class Token is NonInstantiable":
                pass
            else:
                raise e

    @staticmethod
    def modify_init(dec, cls):
        __preinit__ = getattr(dec, "__preinit__", lambda self: None)
        __init__ = cls.__init__
        __postinit__ = getattr(dec, "__postinit__", lambda self: None)

        def init(self, *args, **kwargs):
            use_args = any([DecoratorMetaclass.analyze_inits_parameters(f)[0] \
                            for f in (__preinit__, __init__, __postinit__)])

            use_kwargs = any([DecoratorMetaclass.analyze_inits_parameters(f)[1]\
                              for f in (__preinit__, __init__, __postinit__)])
            if ((not use_args) and len(args) > 0) or \
               ((not use_kwargs) and len(kwargs) > 0):
                __preinit__(self, *args, **kwargs)   # Should rise error
                __init__(self, *args, **kwargs)      # Should rise error
                __postinit__(self, *args, **kwargs)  # Should rise error

            for f in __preinit__, __init__, __postinit__:
                use_args, use_kwargs = \
                    DecoratorMetaclass.analyze_inits_parameters(f)
                if f is object.__init__:
                    f(self)
                elif use_args and use_kwargs:
                    f(self, *args, **kwargs)
                elif use_args:
                    f(self, *args)
                elif use_kwargs:
                    f(self, *kwargs)
                else:
                    f(self)

        cls.__init__ = init

    @staticmethod
    def analyze_inits_parameters(f) -> (bool, bool):
        parameters = list(signature(f).parameters.values())
        use_args = use_kwargs = False
        for p in parameters[1:]:
            if p.kind in (p.VAR_KEYWORD, p.KEYWORD_ONLY,
                          p.POSITIONAL_OR_KEYWORD):
                use_kwargs = True
            if p.kind in (p.VAR_POSITIONAL, p.POSITIONAL_OR_KEYWORD):
                use_args = True
        return use_args, use_kwargs

    @staticmethod
    def apply_post(dec, cls):
        post = [getattr(dec, attr) for attr in dec.__dict__
                if attr.startswith("post_")]
        post.sort(key=lambda x: x.__name__)
        for method in post:
            method(dec, cls)

    @staticmethod
    def extend_docstring(dec, cls):
        docstring = "\nDecorator: {decorator}\n".format(decorator=dec.__name__)
        if dec.__doc__ and dec not in \
                getattr(cls, "__decorators__", {}).values():
            cls.__doc__ += docstring + "\n".join("\t" + line
                                                 for line in dec.__doc__)

    @staticmethod
    def connect_decorator_and_class(dec, cls):
        try:
            if cls not in getattr(dec, "__decorated_classes__", []):
                dec.__decorated_classes__.append(cls)
        except AttributeError:
            dec.__decorated_classes__ = [cls]

        try:
            cls.__decorators__[dec.__name__, dec.__deccounter__] = dec
        except AttributeError:
            cls.__decorators__ = {(dec.__name__, dec.__deccounter__): dec}

    def __call__(dec: DecoratorInterface, cls: Optional = None, **kwargs):

        dec.__deccounter__ = getattr(dec, "__deccounter__", 0)
        dec.__cached_kwargs__ = getattr(dec, "__cached_kwargs__", {})
        dec.__cached_decs__ = getattr(dec, "__cached_decs__", {})
        if not cls:
            return DecoratorMetaclass.get_shadow(dec, **kwargs)
        elif getattr(dec, "METACLASS_RULES", True):
            DecoratorMetaclass.apply_pre(dec, cls)

            DecoratorMetaclass.set_class_variables(dec, cls)
            DecoratorMetaclass.set_hidden_methods(dec, cls)
            DecoratorMetaclass.override_methods(dec, cls)
            if hasattr(dec, "__class_new__"):
                DecoratorMetaclass.override_new(dec, cls)

            DecoratorMetaclass.modify_init(dec, cls)

            DecoratorMetaclass.apply_post(dec, cls)

        DecoratorMetaclass.extend_docstring(dec, cls)

        DecoratorMetaclass.connect_decorator_and_class(dec, cls)

        return cls

