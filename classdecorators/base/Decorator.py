from .DecoratorInterface import DecoratorInterface
from .DecoratorMetaclass import DecoratorMetaclass

class Decorator(DecoratorInterface, metaclass=DecoratorMetaclass):
    """Farci una docstring paccuta che indica come creare un decoratore per una classe."""
    pass