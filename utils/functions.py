from typing import Dict, Tuple


def unpack(d: Dict) -> Tuple:
    t = tuple()
    for item in d.items():
        if isinstance(item, tuple):
            t += item
        elif isinstance(item, dict):
            t += unpack(d)
        elif isinstance(item, list):
            t += tuple(list)
        else:
            t += (item, )
    return t