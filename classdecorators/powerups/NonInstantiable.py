from classdecorators.base.Decorator import Decorator
from classdecorators.base.DecoratorMetaclass import DecoratorMetaclass
from utils.Exceptions import InstantiationError


class NonInstantiable(Decorator):

    def pre_1_store_new(dec, c):
        new = c.__new__
        base = c.__base__
        while base is not object:
            if base in getattr(dec, "__decorated_classes__", []):
                new = base.__class_new__
            base = base.__base__
        c.__class_new__ = new

    def pre_2_connect_dec_and_cls(dec, c):
        DecoratorMetaclass.connect_decorator_and_class(dec, c)

    def pre_3_define_class_new(dec, c):
        def __class_new__(cls, *args, **kwargs):
            if cls in dec.__decorated_classes__:
                raise InstantiationError("class " + cls.__name__ + " is"
                                         " NonInstantiable")
            else:
                return cls.__class_new__(cls)

        setattr(dec, "__class_new__", __class_new__)
