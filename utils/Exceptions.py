class InstantiationError(Exception): pass
class SingletonError(InstantiationError): pass
class CachedError(InstantiationError): pass
class DecorationError(Exception): pass
class InitializationError(Exception): pass