from classdecorators.base.Decorator import Decorator
from utils.functions import unpack
from inspect import signature


class Cached(Decorator):
    __class_variables__ = {"__cache": {}}

    def pre_1_store_new(dec, c):
        new = c.__new__
        base = c.__base__
        while base is not object:
            if base in getattr(dec, "__decorated_classes__", []):
                new = base.__class_new__
            base = base.__base__
        c.__class_new__ = new

    def pre_2_store_init(dec, c):
        c.__class_init__ = c.__init__

    def __class_new__(cls, *args, **kwargs):
        Token = type(cls.__name__, cls.__bases__, dict(cls.__dict__))
        t = Token.__class_new__(Token)
        b = signature(cls.__class_init__).bind(t, *args, **kwargs)
        b.apply_defaults()
        t = tuple()
        for arg, value in b.arguments.items():
            if arg == "self":
                continue
            if arg == "args":
                t += "varargs", value
            elif arg == "kwargs":
                t += unpack(value)
            else:
                t += arg, value
        hash_ = hash(t)
        cache = getattr(cls, "_" + cls.__name__ + "__cache", {})
        if hash_ not in cache:
            instance = cls.__class_new__(cls)
            if cls not in getattr(Cached, "__decorated_classes__", []):
                return instance
            cache[hash_] = instance
        return cache[hash_]
