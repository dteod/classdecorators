from unittest import TestCase

from classdecorators.decorators import DynamicInitialization


class DynamicInitializationImplementationTestUnit(TestCase):
    def setUp(self):
        @DynamicInitialization
        class A:
            pass

        self.A = A

    def test_basic_functionality(self):
        a = self.A(1, 2, 3, arg1=4, arg2=5, arg3=6)

        self.assertEqual(a.arg1, 4, "could not store kwargs")
        self.assertEqual(a.arg2, 5, "could not store kwargs")
        self.assertEqual(a.arg3, 6, "could not store kwargs")
        self.assertEqual(a.args, (1, 2, 3), "could not store args")

    def test_subclass_inherits_being_dynamic(self):
        class B(self.A):
            pass

        b = B(1, 2, 3, arg1=4, arg2=5, arg3=6)

        self.assertEqual(b.arg1, 4, "could not store kwargs")
        self.assertEqual(b.arg2, 5, "could not store kwargs")
        self.assertEqual(b.arg3, 6, "could not store kwargs")
        self.assertEqual(b.args, (1, 2, 3), "could not store args")

    def test_dyninit_doesnt_override_user_defined_init(self):
        @DynamicInitialization
        class A:
            def __init__(self):
                self.a = 1

        a = A(1, 2, 3, a=4)
        self.assertEqual(a.a, 1, "dyninit overwrites __init__")

    def test_dyninit_doesnt_override_kwargs(self):
        @DynamicInitialization
        class A:
            def __init__(self, *args, **kwargs):
                self.args = tuple(reversed(args))
                for k, v in kwargs.items():
                    setattr(self, k, [v])

        a = A(1, 2, 3, a=4)
        self.assertEqual(a.args, (3, 2, 1), "dyninit overwrites __init__")
        self.assertEqual(a.a, [4], "dyninit overwrites __init__")

    def test_giving_args_as_kwarg_need_a_specific_handle(self):
        @DynamicInitialization
        class A:
            def __init__(self, *args, **kwargs):
                self.args = tuple(reversed(args))
                for k, v in kwargs.items():
                    setattr(self, k, [v])
        a = A(1, 2, 3, args=4)
        self.assertEqual(a.args, [4], "args are not overridden")
        self.assertEqual(a._args, (1, 2, 3), "_args are not created")

    def test_using_external_declared_dict(self):
        kwargs = {"a": 1, "b": 2, "c": 3}
        a = self.A(**kwargs)
        self.assertEqual(a.a, 1, "external defined kwargs doesn't work")
        self.assertEqual(a.b, 2, "external defined kwargs doesn't work")
        self.assertEqual(a.c, 3, "external defined kwargs doesn't work")

        b = self.A(kwargs=kwargs)
        c = {}
        c.update(kwargs)
        self.assertEqual(b.kwargs, c, "cannot set dictionary")

    def test_objects_handled_are_the_same_not_copies(self):
        kwargs = {"a": 1, "b": 2, "c": 3}
        a = self.A(**kwargs)
        self.assertTrue(a.a is 1, "external defined kwargs doesn't work")
        self.assertTrue(a.b is 2, "external defined kwargs doesn't work")
        self.assertTrue(a.c is 3, "external defined kwargs doesn't work")

        b = self.A(kwargs=kwargs)
        self.assertTrue(b.kwargs is kwargs, "should be the same")
