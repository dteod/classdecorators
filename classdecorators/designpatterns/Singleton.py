from threading import Lock
from classdecorators.base.Decorator import Decorator
from classdecorators.base.DecoratorMetaclass import DecoratorMetaclass
from utils.Exceptions import SingletonError


class Singleton(Decorator):
    __class_variables__ = {"__instance": None, "__lock": Lock()}

    def pre_1_check_if_not_derived_from_singleton(dec, cls):
        base = cls.__base__
        while base is not object:
            if base in getattr(dec, "__decorated_classes__", []):
                raise SingletonError(cls.__name__ +
                                     " class cannot be inherited by singleton")
            base = base.__base__

    def pre_2_store_new(dec, c):
        try:
            dec.news[c] = getattr(c, "__new__", object.__new__)
        except AttributeError:
            dec.news = {c: getattr(c, "__new__", object.__new__)}

    def pre_3_modify_init(dec, c):
        DecoratorMetaclass.modify_init(dec, c)

    def post_1_store_init(dec, c):
        try:
            dec.inits[c] = getattr(c, "__init__", lambda s, *a, **kw: None)
        except AttributeError:
            dec.inits = {c: getattr(c, "__init__", lambda s, *a, **kw: None)}

    def post_2_set_conditional_init(dec, cls):
        cls.__instanced = False

        def __conditional_init__(self, *args, **kwargs):
            cls = self.__class__
            condition = (cls in getattr(dec, "__decorated_classes__", []) and
                         not cls.__instanced) or \
                        cls not in getattr(dec, "__decorated_classes__", [])
            if condition:
                self.__class__.__instanced = True
                try:
                    dec.inits[self.__class__.__base__](self, *args, **kwargs)
                except KeyError:
                    try:
                        dec.inits[self.__class__](self, *args, **kwargs)
                    except TypeError:
                        dec.inits[self.__class__](*args, **kwargs)
                except TypeError:
                    dec.inits[self.__class__.__base__](*args, **kwargs)

        setattr(cls, "__init__", __conditional_init__)

    def __class_new__(cls, *args, **kwargs):
        # Checks if class is not inherited by singleton. Needed, because
        # inherited classes are not decorated and pre_ doesn't apply.
        baseclass_decorators = getattr(cls.__base__, "__decorators__", None)
        if baseclass_decorators:
            if Singleton.__name__ in [dec[0] for dec in baseclass_decorators]:
                raise SingletonError(cls.__name__ +
                                 " class cannot be inherited by singleton")

        # Singleton instantiation
        instance = "_" + cls.__name__ + "__instance"
        lock = "_" + cls.__name__ + "__lock"
        singleton = getattr(cls, instance, None)
        if not singleton:
            with getattr(cls, lock, Lock()):  # Thread-safe
                singleton = super(cls.__class__, cls).__new__(cls)
                setattr(cls, instance, singleton)
        return singleton

    @classmethod
    def free(dec, cls):
        if not any([issubclass(cls, s)
                    for s in getattr(dec, "__decorated_classes__", [])]):
            raise SingletonError(cls.__name__ +
                                 " does not subclass a singleton")
        base = cls.__base__
        setattr(cls, "__new__", dec.news[base])
        return cls
