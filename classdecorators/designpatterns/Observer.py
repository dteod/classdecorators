from functools import singledispatch as overloaded
from ..base.Decorator import Decorator
from ..powerups.NonInstantiable import NonInstantiable
from utils.ErrorFactory import ErrorFactory

@NonInstantiable
class AbstractSubject(Decorator):
    def pre_1_check_if_has_update(dec, cls):
        if not hasattr(cls, "__update__"):
            raise AttributeError("class " + cls.__name__ + " must implement method update")

    def pre_2_add_register(dec, cls):
        @overloaded
        def register(arg, self):
            if not any(isinstance(arg, Obs) for Obs in self.__valid_observers__):
                raise RegistrationError("impossible to register an unidentified observer")
            else:
                self.observers.append(arg)
                return self

        @register.register
        def register_class(arg: cls, self):
            for observer in cls.__cache:
                self.append(observer)
            return self

        setattr(cls, "_register", register)

    def __preinit__(self, *args, **kwargs):
        self.observers = []

    def register(self, *args):
        for arg in args:
            self._register(arg)
            
    @overload.register(observer)
                assert any(isinstance(observer, Obs) for Obs in self._valid_observers)
            except AssertionError:
                raise ErrorFactory.createError("ObserverRegisterError",
                                               value="not a valid observer for " +
                                                     self.__class__.__name__)
            try:
                observer.register(self)
                self.__observers.append(observer)
            except TypeError:
                raise ErrorFactory.createError("ObserverRegisterError",
                                               value="cannot register an Observer class, only an object")
        return self

    def deregister(self, *args):
        args = args or self.__observers[:]
        for observer in args:
            if observer in self.__observers:
                try:  # Not needed but used to avoid user errors
                    assert any(isinstance(observer, Obs) for Obs in self._valid_observers)
                except AssertionError:
                    raise ErrorFactory.createError("ObserverDeregisteringError",
                                                   value="not a valid observer for " +
                                                         self.__class__.__name__)
            try:
                observer.deregister(self)
            except TypeError:
                raise ErrorFactory.createError("ObserverDeregisteringError",
                                               value="cannot deregister an Observer class, only an object")

    __override_methods__ = {"register": register, "deregister": deregister}

class Observer(Decorator):
    __class_variables__ = {"__cache": [], "__cached": [], "_valid_subjects": []}

    def __preinit__(self, *args, **kwargs):
        self._subjects = []

    def __class_new__(cls, *args, **kwargs): 
    #""" Observers are cached by default so that every autosubject can connect to all the channels (observers).
    #Cached is a good equilibrium: Singleton is too restrictive (non-automatic subjects freedom have 
    #to be considered) and free is to generic (it doesn't make sense that two different broadcasts have 
    #the same content, at least from a computer science perspective: it would be a waste of resources).
    #"""
        if not hasattr(cls, "cache"):
            setattr(cls, "cache", [])

        obj = super(cls.__class__, cls).__new__(cls)
        cls.cache.append(obj)
        return obj
    @singledispatch
    def _register(arg):
        
    def register(self, *args):
        for subject in args:
            try:
                assert any(isinstance(subject, Sub) for Sub in self._valid_subjects)
                if subject not in self._subjects:
                    self._subjects.append(subject)
            except AssertionError:
                raise ErrorFactory.createError("SubjectRegisterError",
                                               value=subject.__class__ + "not a valid subject for " + self.__class__.__name__)
        return self

    def deregister(self, *args):
        args = args or self._subjects[:]
        for subject in args:
            if subject in self._subjects:
                self._subjects.remove(subject)

    def notify_(self, *args, **kwargs):
        for subject in self._subjects:
            subject.update(*args, **kwargs)

    def notify(self, *args, **kwargs):
        self.notify_(*args, **kwargs)

    @classmethod
    def subject(dec, cls):
        try:
            cls._valid_observers.append(dec)
        except AttributeError:
            cls._valid_observers = [dec]

        dec._valid_subjects.append(cls)

        class Subject(Decorator):


        return Subject(cls)

    @classmethod
    def auto(dec, cls):
        try:
            cls._valid_observers.append(dec)
        except AttributeError:
            cls._valid_observers = [dec]

        try:
            cls._automatic_observers.append(dec)
        except AttributeError:
            cls._automatic_observers = [dec]

        dec._valid_subjects.append(cls)

        class AutoSubject(Decorator):

            def __preinit__(self, *args, **kwargs):
                self.__observers = []

            def __postinit__(self, *args, **kwargs):
                for observer in cls._automatic_observers:
                    self.register(*getattr(observer, "_" + observer.__name__ + "__cached"))

            def update(self, *args, **kwargs):
                pass

            def register(self, *args):
                for observer in args:
                    try:
                        assert any(isinstance(observer, Obs) for Obs in self._valid_observers)
                    except AssertionError:
                        raise ErrorFactory.createError("ObserverRegisteringError",
                                                       value="not a valid observer for " +
                                                             self.__class__.__name__)
                    try:
                        observer.register(self)
                        self.__observers.append(observer)
                    except TypeError:
                        raise ErrorFactory.createError("ObserverRegisterError",
                                                       value="cannot register an Observer class, only an object")

            def deregister(self, *args):
                args = args or self.__observers
                for observer in args:
                    if observer in self.__observers:
                        try:
                            assert any(isinstance(observer, Obs) for Obs in self._valid_observers)
                        except AssertionError:
                            raise ErrorFactory.createError("ObserverDeregisteringError",
                                                           value="not a valid observer for " +
                                                                 self.__class__.__name__)
                    try:
                        observer.deregister(self)
                    except TypeError:
                        raise ErrorFactory.createError("ObserverDeregisteringError",
                                                       value="cannot deregister an Observer class, only an object")

            __hidden_methods__ = {"update": update}
            __override_methods__ = {"register": register, "deregister": deregister}

        return AutoSubject(cls)

    __hidden_methods__ = {"notify": notify}
    __override_methods__ = {"register": register, "deregister": deregister,
                            "subject": subject, "auto": auto, "notify_": notify_}
                         

