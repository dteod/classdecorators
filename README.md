Class decorators
================

## Description

This repo contains a package that can be imported to provide ease in implementing the design patterns originally defined in the _Gang of Four_ book, using Python **decorators**. Since Python is a much more dynamic language respect to C++ on which the book was written, not all of the patterns have to be redefined.

The package doesn't limit in implementing design patterns only. Additional functionalities can be added to the function with the same structure, so these are present in the module as well.
___

## Contents

In the module you can find:

1. A metaclass that defines the [rules adopted to implement a class decorator][rules]
2. The Decorator base class you can inherit from to create your own decorator, following the [rules][rules]
3. These decorators you can import and use:
>
* [Singleton][singleton] along with the [Singleton.free][singleton-free] static method

___

## Implementation

The implementation of the decorators uses a paradigm a bit different from Python's standard OOP. This is because of the nature of decorators. If we think about a decorator, we see it as _something_ that is added to a **class**, without changing the **class**' identity: put in simple terms, _if you decide to wear a clock you don't become a clock_, you're just the same person with the ability of knowing the time.

In Python, you can decorate a class in this way:  

``` python

@Decorator
class Something: 
	pass
	
```

that is equivalent to the line

```python 

Something = Decorator(Something)
	
```

This is where the problems come out. _If we instantiate a Decorator and we assign it to the class Something, the class Something becomes an object of Decorator_. And this is the **opposite** behaviour respect to the one defined above.

To bypass this problem, **Decorator's metaclass have to be changed** in agreement with the logic we need. To be short, the `__call__` method of the metaclass must be overridden to permit the **return the argument** it receives instead of an object of Decorator, and some rules to implement the functionalities of the decorator have to be defined.

This leads to a fork in how to proceed. Since you are creating a new behavior for a family of classes, you can proceed how you think is the best way to achieve your results according to your coding skills and your way of thinking. 

### Metaclass rules

These are the rules I decided to implement (_of course these can be improved and adapted to a more general case_). The decorators can be implemented in two ways:

1. implementing the decorator itself. Define an `__init__`, a `__call__` and all the logic you need to perform operations on the defined class (gives you higher flexibility but takes more work and usually more lines)
2. inheriting the Decorator class (or using the DecoratorMetaclass, more verbose), and following the rules down here.

The rules are thought as a generic way to perform a fixed set of operations. Usually those simplifiy the logic of the decorator itself and reduce the lines of code needed, depending on the functionality you want to carry via the decorator.

* if a method starts with `pre_`, then it is called before other operations; the premethods are executed in alphabetic order (so you can name the methods `pre_1_name1`, `pre_2_name2` and so on to control execution). Takes in input dec (the decorator class itself) and cls (the decorated class). Used to perform function checks, deletes, and whatsoever
* the `__class_variables__` variable holds a dictionary with as keys the names of the class variables the decorated class receives taking advantage of the decorator, along with a default initial value that is copied in the class. Private variables are included too and can be declared in a general form (`__classvariable` instead of `_clsname__classvariable`). Notice that these variables are not overridden (and the decorator that does that should **NOT** behave in this way, unless you want it to break user code). The `__decorators__` dictionary is set by the metaclass and updated correspondingly to keep track of the decorators applied to it, so there is no need for you to think about this.
* the `__hidden_methods__` variable holds a dictionary in which the values are the methods the decorated class receives from the decorator and the keys are the names with which the class and its instances access the method; the methods can be decorated with `@classmethod` and `@staticmethod` as well and those behaviours are preserved. If the class implements methods with the same signature on its own, then the decorator methods are hidden.
* the `__override_methods_` variable is a dictionary that contains methods that are implemented by the decorated class, even if it declares a method with the same signature (notice that the method name in the class is the corresponding key in the dictionary). Be careful about these method since they can break the user code.
* the `__class_new__` method contains operations to be performed before class instantiation (the `__new__` method). Since this usually returns the instantiated object, returning an instance of the decorated class is mandatory to avoid None problems. Overrides the class' `__new__` method.
* the `__preinit__` and `__postinit__` methods are called before and after the class' `__init__`, respectively. Instance variables can be defined in these methods (even in this case of private variables, that can be declared in a general way); prefer preinit for doing this, since postinit will overwrite instance variables defined in the class initializator, if a check is not performed.
* if a method starts with `post_`, the metaclass will apply them after the other operations, following an alphabetic order of the postmethods' names. Takes dec and cls as inputs.

After the class is modified, three operations are performed by the metaclass by default:

1. The decorated class is updated with a track of the decorator that modified it in the `__decorators__` dictionary
2. The decorator is updated with a track of the decorated class in the `__decorated_classes__` dictionary
3. The decorator's docstring is added to the class' docstring, with a reference to
	
In my opinion, these are general enough to implement every possible decorator, but I'm a human and I wouldn't bet on it. If you want to discuss it, please contact me at [dteodonio93@gmail.com](mailto:dteodonio93@gmail.com) .

TODO: think about decorators' interactions.
___

## Decorators

The following decorators have been `implemented` or _tested_. Implementation is discussed in the corresponding module docstring, here follows a brief description on how to use it a link to the module itself.

### [_`Singleton`_][singleton-link]

The singleton pattern is the simplest pattern you can think of. It is a way to make a class instantiable only once: whenever the class is instantiated again, it always returns the same instance.

Usages (source <https://en.wikipedia.org/wiki/Singleton_pattern>):

* The abstract factory, builder, and prototype patterns can use singletons in their implementation.
* Facade objects are often singletons because only one facade object is required.
* State objects are often singletons.
* Singletons are often preferred to global variables because of lazy initialization/allocation and they don't pollute the namespace.

#### [_Singleton.free_][singleton-link]

Inherit a Singleton usually leads to problems in languages like Java because of the private constructors that cannot be 
inherited. In our case though, the usage of a decorator leads to the possibility to inherit the Singleton as well.

Problems rise in the definition of the derived class. Is the derived class a singleton, or not? In the traditional way 
of seeing the topic, a Singleton cannot be inherited. This is because accessing the `__new__` method you could be able
to create a second singleton as well, and there would be no sense to define it as a singleton.
You would create the same object, giving it a different behaviour respect to the "old" one (that is actually the same).

To maintain Python's flexibility and allow subclassing, you can use another decorator to free the subclass from
the burden of being a singleton and make the class instantiable again. @Singleton-free makes the object instantiable multiple times again:

```python

```

### [`Cached`][singleton-link]

[Cached][singleton-link] is the class version of the memoize decorator. I didn't care about if memoize worked with classes, even because it would have
been quite odd to store object instances in a function-local variable; so I decided to implement it in a Decorator subclass.

Cached sees if you already instantiated your class with the same parameters and, if it does, it returns a cached instance instead of creating a new one.
It may be useful to reduce the amount of memory in the code and to remove the initialization time of cached objects, at the cost of a tuple search in a
cache.

Not tested at 100% yet; need to see the interactions within other decorators.

#### [`Cached.free`][singleton-link]

Same problem faced with the [singleton decorator][singleton-free], solved at the same way. Cached.free and Singleton.free are actually described by
the same function (probably I have to extrapolate it to avoid code repetition...)


### [`Observer`][singleton-link]
#### [`observer.subject`][singleton-link]
#### [`observer.auto`][singleton-link]
### [`NonInstantiable`][singleton-link]
### [`DynamicInstantiation`][singleton-link]
### Multiton
Essentially a cached object but with labels instead of arguments as keys
### Branched
Corresponds to the lazy_thunk found on the web.
### ParallelMapper
You feed it with arguments and performs an `operation(arg)` automatically in parallel. This is the usage:

```python
args = SomethingThatGivesYouDataToBeProcessedIndependently.getData()

@ParallelMapper(method="thread")
class Solver:
    def operation(self, argument):
        pass  # operation to be performed
        
Solver(args)  # performs operation in parallel

# Operation should also be dinamically injected:
def operation(argument): pass
Solver(args, operation=operation)

```

### Factory
#### factory.product(tag=None)
### AbstractFactory
#### abstractFactory.family(tag=None)
#### tag.product(tag=None)
### Builder
### Prototype
### Memento
#### memento.state
### PDBC
### File

[rules]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/###metaclass-rules
[singleton]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/###singleton
[singleton-free]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/####singleton.free
[cached]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/###cached
[cached-free]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/####cached.free
[observer]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/###observer
[observer-subject]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/####observer.subject
[observer-auto]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/####observer.auto
[noninstantiable]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/###noninstantiable
[dynamicinstantiation]: https://bitbucket.org/dteod/classdecorators/src/master/README.md/###dynamicinstantiation
[singleton-link]: https://bitbucket.org/dteod/classdecorators/src/master/classdecorators/decorators.py
