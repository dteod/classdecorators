from unittest import TestCase

from classdecorators.decorators import NonInstantiable
from utils.Exceptions import InstantiationError


class NonInstantiableImplementationTestUnit(TestCase):
    def setUp(self):
        @NonInstantiable
        class A:
            def __init__(self):
                pass

        self.A = A

    def test_marked_class_is_not_instantiable(self):
        with self.assertRaises(InstantiationError,
                               msg="should not instantiate"):
            self.A()

    def test_inherited_class_is_instantiable(self):
        class B(self.A):
            pass

        B()

    def test_inherited_class_can_call_parent_init(self):
        @NonInstantiable
        class A:
            def __init__(self):
                self.a = 1

        class B(A):
            def __init__(self):
                super().__init__()

        class C(A):
            pass

        B()
        C()

        self.assertTrue(B().a is 1, "deleted superclass initialization")
        self.assertTrue(C().a is 1, "doesn't call superclass initialization")

    def test_inherited_class_can_be_uninstantiable(self):
        @NonInstantiable
        class B(self.A):
            def __init__(self):
                pass
        
        with self.assertRaises(InstantiationError,
                               msg="should not instantiate"):
            B()
