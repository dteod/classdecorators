from typing import Type
from classdecorators.powerups.NonInstantiable import NonInstantiable

@NonInstantiable
class ErrorFactory:
    @staticmethod
    def createError(name: str, value: str=None, base: Type[Exception]=Exception):
        name = name
        bases = tuple([base])
        dict = {}
        error = type(name, bases, dict)
        if not value:
            return error
        else:
            return error(value)

