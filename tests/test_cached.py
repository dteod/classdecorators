from unittest import TestCase

from classdecorators.decorators import Cached


class CachedImplementationTestUnit(TestCase):
    def setUp(self):
        @Cached
        class A:
            def __init__(self, *args, **kwargs):
                self.args = args[:]
                for key in kwargs:
                    setattr(self, key, kwargs[key])

        self.A = A

        self.args = (1, 2, 3)
        self.kwargs = {"key1": 1, "key2": 2, "key3": 3}
        self.cached = A(*self.args, **self.kwargs)

    def test_cached_class_instances_with_same_params_are_the_same(self):
        a = self.A(1)
        b = self.A(1)
        c = self.A(*self.args, **self.kwargs)
        self.assertTrue(a is b, "did not cache instances")
        self.assertTrue(c is self.cached, "did not cache instances")

    def test_cached_signature_works_at_anytime(self):
        @Cached
        class X:
            def __init__(self):
                pass

        x1 = X()
        with self.assertRaises(TypeError, msg="should not instantiate"):
            x2 = X(1)
            x3 = X(1, key=0)

        self.assertTrue(x1 is X(), "did not cache properly")

        @Cached
        class Y:
            def __init__(self, arg):
                pass

        y2 = Y(1)
        with self.assertRaises(TypeError, msg="should not instantiate"):
            y1 = Y()
            y3 = Y(1, key=0)

        self.assertTrue(y2 is Y(1), "did not cache properly")

        @Cached
        class Z:
            def __init__(self, arg, key=1):
                pass

        z2 = Z(1)
        z3 = Z(1, key=0)
        with self.assertRaises(TypeError, msg="should not instantiate"):
            z1 = Z()
            z1.key

        self.assertTrue(z2 is Z(1), "did not cache properly")
        self.assertTrue(z2 is not z3, "did not cache properly")
        self.assertTrue(z3 is Z(1, 0), "did not cache properly")
        self.assertTrue(z3 is not Z(1, 1), "should pass 100%")

    def test_inherited_class_is_not_cached(self):
        @Cached
        class A:
            pass

        class B(A):
            pass

        self.assertTrue(B() is not B(), "should not be the same")

    def test_inherited_class_can_be_cached(self):
        @Cached
        class A:
            pass
            
        @Cached
        class B(A):
            pass

        self.assertTrue(B() is B(), "should be the same")

    def test_nephew_class_can_be_cached(self):
        @Cached
        class A:
            pass

        class B(A):
            pass

        @Cached
        class C(B):
            pass
        self.assertTrue(C() is C(), "should be the same")

    def test_multiple_inheritance_does_not_matter(self):
        @Cached
        class A:
            pass

        class B(A):
            pass

        @Cached
        class D:
            pass

        @Cached
        class C1(D, B):
            pass
        self.assertTrue(C1() is C1(), "should be the same")

        class C2(D, B):
            pass
        self.assertTrue(C2() is not C2(), "should not be the same")

        @Cached
        class C3(B, D):
            pass
        self.assertTrue(C3() is C3(), "should be the same")

        class C4(B, D):
            pass
        self.assertTrue(C4() is not C4(), "should not be the same")

    def test_cached_object_remain_even_in_scope(self):
        @Cached
        class A:
            def __init__(self, arg):
                self.arg = arg
        def f():
            return A(1)
        
        self.assertTrue(A(1) is f(), "cached fails with different scopes")

    def test_cached_object_with_different_parameters_are_not_the_same(self):
        @Cached
        class A:
            def __init__(self, arg=0):
                self.a = arg

        self.assertTrue(A() is not A(1), "same objects even with different"
                                         " arguments")

    def test_one_object_with_default_arg_other_with_not_default(self):
        @Cached
        class A:
            def __init__(self, arg=0):
                self.a = arg

        self.assertTrue(A() is A(0), "objects differ if default arg")

    def test_more_stress_in_args_kwargs_handling(self):
        @Cached
        class A:
            def __init__(self, arg, arg2=0, **kwargs):
                pass

        a1 = A(1)
        a2 = A(1, 0)
        
        b1 = A(1, 0, kw1=0)
        b2 = A(1, arg2=0, kw1=0)
        
        self.assertTrue(a1 is a2, "review argument parsing")
        self.assertTrue(b1 is b2, "review argument parsing")
        
        c1 = A(1, 1, kw1=0)
        c2 = A(1, arg2=1, kw2=0)
        self.assertTrue(c1 is not c2, "takes kw2 like kw1")

    def test_using_cached_object_with_default_argument(self):
        @Cached
        class Cache:
            def __init__(self, arg=0):
                self.arg = arg

        class A:
            def __init__(self, test, cache1=Cache(), cache2=Cache()):
                test.assertTrue(cache1 is cache2, "not working as default")

        A(self)

    def test_different_object_with_same_arguments_are_not_the_same(self):
        @Cached
        class A:
            pass

        @Cached
        class B:
            pass

        class C:
            pass

        self.assertTrue(A() is not B(), "problem in caching arguments")
        self.assertTrue(A() is not C(), "problem in caching arguments")
        self.assertTrue(B() is not C(), "problem in caching arguments")