from unittest import TestCase, skip

from classdecorators.decorators import Singleton
from utils.Exceptions import SingletonError
from tests.utilities import print


class SingletonImplementationTestUnit(TestCase):
    def setUp(self):
        # If I setup a singleton here, then all the tests will share it. LOL.
        pass

    def test_there_is_only_one(self):
        @Singleton
        class Single:
            def __init__(self, *args, **kwargs):
                for key in kwargs:
                    setattr(self, key, kwargs[key])

        self.assertTrue(Single() is Single(),
                        "two different instances created")

    def test_double_instantiation_fails(self):
        @Singleton
        class Single:
            def __init__(self, *args, **kwargs):
                for key in kwargs:
                    setattr(self, key, kwargs[key])

        a = Single(kw1=1, kw2=2, kw3=3)
        b = Single(kw1=4, kw2=5, kw3=6, kw4=7)

        self.assertEqual(a.kw1, 1, "initialization for b started")
        self.assertEqual(a.kw2, 2, "initialization for b started")
        self.assertEqual(a.kw3, 3, "initialization for b started")
        with self.assertRaises(AttributeError,
                               msg="singleton should not have kw4"):
            self.assertEqual(a.kw4, b.kw4)

    def test_single_instantiations_in_different_scopes(self):
        @Singleton
        class Single:
            def __init__(self, *args, **kwargs):
                for key in kwargs:
                    setattr(self, key, kwargs[key])

        def define_in_scope():
            Single(a=1)

        define_in_scope()
        s = Single(a=2)

        self.assertNotEqual(s.a, 2, "conflict between scopes: not a singleton")

    def test_inheritance_from_a_singleton_not_instantiable(self):
        @Singleton
        class A:
            def __init__(self):
                self.a = 1

        with self.assertRaises(SingletonError,
                               msg="subclass w/o free instantiable"):
            class B(A):
                def __init__(self):
                    super().__init__(self)
                    self.b = 2
            B()

    def test_inherited_class_with_Singleton_free_is_not_a_singleton(self):
        @Singleton
        class A:
            def __init__(self):
                self.a = 1

        @Singleton.free
        class B(A):
            def __init__(self):
                super().__init__(self)
                self.b = 2

        b1 = B()
        b2 = B()
        self.assertTrue(not(b1 is b2), "inherited class is a singleton")

    def test_inherited_class_inherits_init(self):
        print()

        @Singleton
        class A:
            def __init__(self):
                self.a = 1

        @Singleton.free
        class B(A):
            def __init__(self):
                super().__init__(self)
                self.b = 2

        b1 = B()
        self.assertEqual(b1.a, 1, "__init__ not inherited by subclass")

    def test_inherited_class_inherits_init_always(self):
        @Singleton
        class A:
            def __init__(self):
                self.a = 1

        A()

        @Singleton.free
        class B(A):
            def __init__(self):
                super().__init__(self)
                self.b = 2

        b1 = B()
        self.assertEqual(b1.a, 1, "__init__ not inherited by subclass")
        self.assertEqual(b1.b, 2, "__init__ not inherited by subclass")

    def test_inherited_class_can_access_singleton_instance(self):
        @Singleton
        class A:
            def __init__(self):
                self.a = 1

        A()  # notice this. Sets A.__init__ to __conditional_init__

        @Singleton.free
        class B(A):
            def __init__(self):
                super().__init__(self)
                self.b = 2

        b1 = B()
        self.assertTrue(hasattr(b1, "_A__instance"))

    def test_subclass_can_access_instance_even_if_not_defined(self):
        @Singleton
        class A:
            def __init__(self):
                self.a = 1

        @Singleton.free
        class B(A):
            def __init__(self):
                super().__init__(self)
                self.b = 2

        b1 = B()
        self.assertTrue(hasattr(b1, "_A__instance"))

    def test_inherited_class_cannot_change_singleton_instance(self):
        @Singleton
        class A:
            def __init__(self):
                self.a = 1

        a = A()

        @Singleton.free
        class B(A):
            def __init__(self):
                super().__init__(self)
                self.b = 2

        b1 = B()
        self.assertTrue(b1._A__instance is a)
        b1._A__instance = b1
        self.assertTrue(a is A())
        self.assertTrue(b1 is not B())

    def test_subclass_does_not_call_singleton_init_automatically(self):
        @Singleton
        class A1:
            def __init__(self):
                self.a = 1

        @Singleton.free
        class B1(A1):
            def __init__(self):
                self.b = 2
        A1()

        b1 = B1()
        with self.assertRaises(AttributeError,
                               msg="should not able to access member a"):
            self.assertNotEqual(b1.a, 1, "A should not initialize")

        @Singleton
        class A2:
            def __init__(self):
                self.a = 3
        A2()

        @Singleton.free
        class B2(A2):
            def __init__(self):
                super().__init__(self)
                self.b = 4

        b2 = B2()
        self.assertEqual(b2.a, 3, "A2 initialized, but wrong value shown")
        self.assertEqual(b2.b, 4, "B2 initialization failed")

    def test_cannot_set_generic_class_as_free_randomly(self):
        with self.assertRaises(SingletonError,
                               msg="should not be able to apply free"):
            @Singleton.free
            class B:
                def __init__(self):
                    self.b = 2

    def test_double_singleton_tag_works(self):
        @Singleton
        @Singleton
        class A:
            pass

        self.assertTrue(A() is A(), "double decoration does not work")

    def test_double_singleton_free_tag_does_not_work(self):
        @Singleton
        class A:
            pass

        with self.assertRaises(SingletonError, msg="can free twice"):
            @Singleton.free
            @Singleton.free
            class B:
                pass

    def test_free_and_then_retag_does_not_work_as_well(self):
        @Singleton
        class A:
            pass

        with self.assertRaises(SingletonError,
                               msg="inherit from a singleton "
                                   "as a singleton is possible"):
            @Singleton
            @Singleton.free
            class B(A):
                pass
            B()

    def test_tag_subclass_as_singleton_is_possible(self):
        @Singleton
        class A:
            pass

        with self.assertRaises(SingletonError,
                               msg="inherit from a singleton is possible"):
            @Singleton
            class B(A):
                pass

    def test_cannot_define_singleton_children_even_if_nephew(self):
        @Singleton
        class A1:
            pass
        A1()

        @Singleton.free
        class B1(A1):
            pass
        B1()

        with self.assertRaises(SingletonError,
                               msg="nephew class can be a singleton"):
            @Singleton
            class C1(B1):
                pass
            C1()

        @Singleton
        class A2:
            pass

        @Singleton.free
        class B2(A2):
            pass

        with self.assertRaises(SingletonError,
                               msg="nephew class can be a singleton"):
            @Singleton
            class C2(B2):
                pass

    @skip
    def test_Singleton_is_thread_safe(self):
        # I have no ideas on how to perform this test.
        pass
