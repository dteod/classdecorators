from abc import abstractmethod
from copy import deepcopy
from typing import Type


class Incapsulated(Decorator):
    """Note for the user: just declare the encapsulated variables with a dunder."""


# @Cached
# class A:
#     def __init__(self, *args, **kwargs):
#         print("init: " + str(self.__instance))
#
#
# @Cached
# class B(A):
#     def __init__(self):
#         self.__instance = 1
#         print(self.__instance)
#
# @Cached
# class C(B):
#     def __init__(self, i=0):
#         super(C, self).__init__()
#         self.i = i
#
# c1 = C()
# c2 = C()
# c3 = C(1)
# c4 = C(2)
#
# print(c1)
# print(c2)
# print(c3)
# print(c4)

# A = Observer(A)

# a = A(parametri)

@Observer
class A:
    """Deve """
    def __init__(self, *args, **kwargs):
        self.name = args[0]
        print("init: " + self.__class__.__name__)


@Observer
class AA:
    """Se vuole fare qualcosa prima di notificare i subject, deve implementare il metodo _notify al termine."""
    def __init__(self, *args, **kwargs):
        print("init: " + self.__class__.__name__)

    def notify(self, *args, **kwargs):
        print("notification by AA")
        self.notify_(*args, **kwargs)


@AA.subject
@A.auto
class B:
    """Deve implementare metodo update(self, *args, **kwargs)"""
    def __init__(self, name: str):
        self.name = name
        print(self.name + " initialized")

    def update(self, *args, **kwargs):
        print(self.name + " updated")
        print(args, kwargs)


class C(B):
    """Una sottoclasse di subject e' ancora subject degli observer della superclasse. Non sovrascrivere init, o
    non prende piu gli observer"""
    def __init__(self, name):
        super().__init__(name)

    def update(self, *args, **kwargs):
        print(self.name + " updated")
        print(args, kwargs)

a = A("a")

aa = AA("aa")

b1 = B("b1")
b1.register(a)

b2 = B("b2")
b2.register(aa)

c1 = C("c1")
c1.register(a, aa)

c2 = C("c2")

print("a notifies")
a.notify(1, 2.3, [4,5], sei=7)
print("aa notifies")
aa.notify(fuck="you")



a.deregister()
aa.deregister(c1)
print("a notifies")
a.notify(1, 2.3, [4,5], sei=7)
print("aa notifies")
aa.notify(fuck="you")



#
# @DynamicInitialization
# class A0:
#     def __init__(self, *args):
#         try:
#             self.i = args[0]
#         except IndexError as e:
#             print(e)
#
#
# @DynamicInitialization
# class A:
#     pass
#
# a0 = A0()
# a1 = A(1)
# a2 = A0(att1="1")
#
# print(a0.__dict__)
# print(a1.__dict__)
# print(a2.__dict__)


pass

# Decorators defined so far:
#   - Singleton
#      - Singleton.free
#   - Cached
#      - Cached.free
#   - NonInstantiable
#   - DynamicInitialization
#   - Observer
#     with A = Observer(A):
#        - A.subject
#        - A.auto

# TODO: Counter, Incapsulated
