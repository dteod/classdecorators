from unittest import TextTestRunner, TestLoader, main
from tests.test_base import *
from tests.test_singleton import *
from tests.test_cached import *
from tests.test_dynamic_initialization import *
from tests.test_non_instantiable import *
from tests.test_observer import *

if __name__ == "__main__":
    print("Test suite started.")
    main(verbosity=3)
