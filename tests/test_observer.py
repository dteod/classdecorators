from unittest import TestCase, skip

from classdecorators.designpatterns.Observer import Observer


class TestException(Exception):
    pass


class ObserverImplementationTestUnit(TestCase):
    def setUp(self):
        @Observer
        class Obs:
            pass

        @Obs.subject
        class Sub:
            def update(self, *args, **kwargs):
                pass

        @Obs.auto
        class Auto:
            def update(self, *args, **kwargs):
                pass

        self.Obs = Obs
        self.Sub = Sub
        self.Auto = Auto

    def test_observer_is_instantiable(self):
        self.Obs()

    def test_Observer_class_is_iterable(self):
        len(self.Obs)

    def test_observer_is_tracked(self):
        self.assertTrue(self.Obs() in Observer, "object not tracked")
        
    def test_observer_deletes_tracks_when_out_of_scope(self):
        @Observer
        class A:
            pass

        a = A()
        b = A()
        self.assertEqual(len(A), 2, "observer does not track objects")

        def f():
            b = A()
            self.assertEqual(len(A), 3, "observer does not track objects")
            self.assertEqual(b in A)
        f()
        self.assertEqual(len(A), 2, "observer does not track objects")
        del a
        del b
        self.assertEqual(len(A), 0, "observer does not track objects deletion")

    def test_observer_is_not_cached(self):
        self.assertTrue(self.Obs() is not self.Obs(), "objects are cached")

    def test_observer_tracks_its_subjects(self):
        @Observer
        class A:
            pass

        @A.subject
        class B:
            pass

        self.assertTrue(B in A.subject_classes)

    def test_observer_implements_notify(self):
        @Observer
        class A:
            pass

        A().notify()

    def test_subject_register_returns_modified_subject(self):
        a = self.Obs()
        b = self.Sub()
        c = b.register(a)
        self.assertTrue(b is c, "register does not return subject")

    def test_beforeNotifications_is_called_with_notify(self):
        @Observer
        class A:
            def beforeNotification(self, *args, **kwargs):
                raise TestException()

        @A.subject
        class B:
            pass

        a = A()
        b = B().register(a)

        with self.assertRaises(TestException, msg="beforeNotification not called"):
            a.notify()

    def test_observer_implements_afterNotifications(self):
        @Observer
        class A:
            def afterNotification(self, *args, **kwargs):
                raise TestException()

        @A.subject
        class B:
            pass

        a = A()
        b = B().register(a)

        with self.assertRaises(TestException, msg="beforeNotification not called"):
            a.notify()

    def test_subject_must_implement_update_method(self):
        with self.assertRaises(AttributeError, msg="subject must implement method update(self, *args, **kwargs)"):
            @Observer
            class A:
                pass
            
            @A.subject
            class B:
                pass

    def test_subject_can_register_to_observer(self):
        a = self.Obs()
        b = self.Sub()
        b.register(a)
        self.assertTrue(a in b.__observers__)
        self.assertTrue(b in a.subjects)
        
    def test_subject_cannot_register_to_generic_class(self):
        a = self.Sub()
        class B:
            pass
        b = B()
        with self.assertRaises(RegistrationError, msg="subject can register to random classes"):
            a.register(b)
        
    def test_subject_can_unregister(self):
        a = self.Obs()
        @self.Obs.subject
        class S:
            def update(self, *args, **kwargs):
                raise TestException()
        
        s = S().register(a)
        with self.assertRaises(TestException, msg="observer does not notify or object unable to register"):
            a.notify()
        
        s.unregister()  # void argument: unregister from every observer
        a.notify()  # Ok, no exception raised


        s = s.register(a)
        with self.assertRaises(TestException, msg="observer does not notify or subject unable to register"):
            a.notify()
        s.unregister(a)  # observer as argument: unregister only from that
        a.notify()  # Ok, no exception raised
        
    def test_subject_can_unregister_only_from_registered_observers(self):
        s = self.Sub()
        a = self.Obs()
        b = self.Obs()
        
        s.register(a)
        with self.assertRaises(RegistrationError, msg="can unregister from observer not observing"):
            s.unregister(b)

    def test_unregistering_with_class_removes_all_obs_of_that_class(self):
        @Observer
        class A1: pass
        
        @Observer
        class A2: pass
        
        @A2.subject
        @A1.subject
        class B:
            def update(self, *args, **kwargs): pass
        
        a11 = A1()
        a12 = A1()
        a2 = A2()
        b = B().register(a11, a12, a2)
        
        self.assertTrue(len(b.observers), 3, "register with varargs does not work")
        
        b.unregister(A1)
        self.assertTrue(len(b.observers), 1, "could not unregister with class")

    def test_subject_throws_exception_if_failed_unregister(self):
        s = self.Sub()
        with self.assertRaises(RegistrationError, msg="exception raise for void unregister not implemented"):
            s.unregister()
        
        s.register(self.Obs())
        with self.assertRaises(RegistrationError, msg="exception raise for wrong unregister not implemented"):
            s.unregister(self.Obs())

    def test_subject_is_not_cached(self):
        self.assertTrue(self.Sub() is not self.Sub(), "subjects are cached")

    def test_subject_subclass_is_not_a_subject(self):
        class S(self.Sub):
            pass

        s = S()
        o = self.Obs()
        with self.assertRaises(RegistrationError, msg="can register subjects subclasses"):
            s.register(o)

    def test_subject_can_register_to_subclass_of_observer(self):
        class OChild(self.Obs):
            pass

        s = self.Sub()
        sub_o = OChild()
        s.register(sub_o)

    def test_auto_registers_automatically(self):
        o1 = self.Obs()
        o2 = self.Obs()
        a = self.Auto()
        
        self.assertEqual(len(a.observers), 2, "auto does not autoregister to all observers")

    def test_auto_can_unregister_from_observer(self):
        o1 = self.Obs()
        o2 = self.Obs()
        a = self.Auto()
        
        a.unregister(o1)
        self.assertEqual(len(a.observers), 1, "auto cannot unregister")

    def test_auto_can_use_varargs(self):
        o1 = self.Obs()
        o2 = self.Obs()
        o3 = self.Obs()
        a = self.Auto()

        a.unregister(o1, o2)
        self.assertEqual(len(a.observers), 1, "auto cannot unregister with varargs")

    def test_auto_registers_even_after_observer_instantiation(self):
        a = self.Auto()
        o1 = self.Obs()
        o2 = self.Obs()
        o3 = self.Obs()

        self.assertEqual(len(a.observers), 3, "auto does not register to observers defined after it")

    def test_auto_can_register_from_his_observers_only(self):
        o1 = self.Obs()
        o2 = self.Obs()
        @Observer
        class O:
            pass
        a = self.Auto()

        with self.assertRaises(RegistrationError, msg="auto can register to random observers"):
            a.register(O())
        with self.assertRaises(RegistrationError, msg="auto can register to random objects"):
            a.register(object())

    def test_auto_autoregisters_to_all_his_observers(self):
        @Observer
        class O1:
            pass
        @Observer
        class O2:
            pass
        @O1.auto
        @O2.auto
        class S:
            def update(self, *args, **kwargs):
                pass
        o11 = O1()
        o12 = O1()
        o21 = O2()
        o22 = O2()
        s = S()

        self.assertEqual(s.observers, 4, "auto of two observers does not autoregister to both")

    def test_auto_can_use_class_to_unregister_from_those_obs(self):
        @Observer
        class O1:
            pass
        @Observer
        class O2:
            pass
        @O1.auto
        @O2.auto
        class S:
            def update(self, *args, **kwargs):
                pass
        o11 = O1()
        o12 = O1()
        o13 = O1()
        o21 = O2()
        o22 = O2()
        s = S().unregister(O1)

        self.assertEqual(len(s.observers), 2, "auto unregister with class does not work")
        
    def test_auto_can_use_void_to_unregister_from_everything(self):
        @Observer
        class O1:
            pass
        @Observer
        class O2:
            pass
        @O1.auto
        @O2.auto
        class S:
            def update(self, *args, **kwargs):
                pass
        o11 = O1()
        o12 = O1()
        o21 = O2()
        o22 = O2()
        s = S()

        self.assertEqual(s.observers, 4, "auto of two observers does not autoregister to both")
        
        s.unregister()
        self.assertEqual(s.observers, 0, "unregister with void does not work on auto")

    def test_class_cannot_be_both_subject_and_auto(self):
        with self.assertRaises(DecorationError, msg="actually decorating both with auto and subject works"):
            @self.Obs.subject
            @self.Obs.auto
            class Undefined:
                def update(self, *args, **kwargs):
                    pass

    def test_class_can_be_subject_and_auto_for_two_observers(self):
        @Observer
        class O1:
            pass

        @Observer
        class O2:
            pass

        @O1.subject
        @O2.auto
        class Subject:
            def update(self, *args, **kwargs):
                pass

        o11 = O1()
        o12 = O1()
        o13 = O1()
        o21 = O2()
        o22 = O2()
        s = Subject()
        self.assertEqual(len(s.observers), 2, "not an auto for observer O2")
        
        s.register(o11, o12, o13)
        self.assertEqual(len(s.observer), 5, "could not register observers")

        s.unregister(o11)
        self.assertEqual(len(s.observer), 4, "error in unregistering")
        
        s.unregister(o12, o21)
        self.assertEqual(len(s.observer), 2, "error in using varargs for unregistering")

        s.unregister(O2)
        self.assertEqual(len(s.observer), 1, "error in using class for unregistering")

        s.register(o1, o2, o3)
        s.unregister()
        self.assertEqual(len(s.observers), 0, "error in using void for unregistering")

    def test_observer_can_un_and_register_its_subjects(self):
        @Observer
        class O:
            pass

        @O.subject
        class S:
            def update(self, *args, **kwargs):
                kwargs["counter"].append(0)
        
        o = O()
        s1 = S().register(o)
        s2 = S().register(o)
        s3 = S().register(o)
        o.unregister(s1)
        l = []
        o.notify(counter=l)
        self.assertEqual(len(l), 2, "observer cannot unregister subjects or update not called on subjects")
        
        l = []
        @O.auto
        class A:
            def update(self, *args, **kwargs):
                kwargs["counter"].append(0)
        a1 = A()
        a2 = A()
        a3 = A()
        o.unregister(S)
        o.notify(counter=l)
        self.assertEqual(len(l), 3, "observer cannot unregister subjects with class, or update not called on subjects")
        
        o.register(s1, s2)
        o.unregister()
        l = []
        o.notify(counter=l)
        self.assertEqual(len(l), 0, "observer cannot unregister subjects with void")

    @skip
    def test_observer_cannot_unregister_subjects_not_registered(self):
        o = self.Obs()
        s = self.Sub()
        with self.assertRaises(RegistrationError, msg="error: can unregister with void even without subjects"):
            o.unregister()
        a = self.Auto()
        with self.assertRaises(RegistrationError, msg="error: can unregister with varargs if one is not registered"):
            o.unregister(s, a)
        with self.assertRaises(RegistrationError, msg="error: can unregister with class even if no subjects of that class are listening"):
            o.unregister(self.Sub)
        o.unregister(a)

    def test_observer_cannot_register_non_subject_objects(self):
        o = self.Obs()
        class A:
            pass
        a = A()
        with self.assertRaises(RegistrationError, "observer can register random classes as observers"):
            o.register(a)
    @skip
    def test_subject_tracks_its_observers(self):
        @Observer
        class O1:
            pass

        @Observer
        class O2:
            pass

        @O1.subject
        @O2.subject
        class Sub:
            def update(self, *args, **kwargs):
                pass

        o1 = O1()
        o2 = O1()
        o3 = O2()
        s = Sub().register(o1, o2, o3)
        self.assertTrue(o1 in s.observers, "observers are not tracked by subjects")
        self.assertTrue(o2 in s.observers, "observers are not tracked by subjects")
        self.assertTrue(o3 in s.observers, "observers are not tracked by subjects")

    def test_observer_subclass_is_an_observer(self):
        @Observer
        class Father:
            pass
        
        class Child(Father):
            pass

        @Child.subject
        class A:
            def update(self, *args, **kwargs):
                raise TestException()

        o = Child()
        s = A()
        s.register(s)
        with self.assertRaises(TestException, "child does not call update on its subjects"):
            o.notify()

    def test_observer_subclass_inherits_the_subjects(self):
        @Observer
        class Father:
            pass
        
        class Child(Father):
            pass

        @Father.subject
        class A:
            def update(self, *args, **kwargs):
                raise TestException()

        o = Child()
        s = A()
        s.register(s)  # Should work
        with self.assertRaises(TestException, "child does not call update on its subjects"):
            o.notify()

    def test_subject_can_use_class_to_register_observers_of_that_class(self):
        @Observer
        class O1:
            pass
        
        @Observer
        class O2:
            pass
        
        @O1.subject
        @O2.subject
        class Sub:
            def update(self, *args, **kwargs):
                pass

        o11 = O1()
        o12 = O1()
        o13 = O1()
        o21 = O2()
        o22 = O2()
        s = Sub()
        
        s.register(o11)
        self.assertEqual(len(s.observers), 1, "cannot register o11")
        s.register(O1)
        self.assertEqual(len(s.observers), 3, "cannot use class to register O1 observers")
        s.unregister()
        s.register(O2)
        self.assertEqual(len(s.observers), 2, "cannot use class to register O2 observers")

